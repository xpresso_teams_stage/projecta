import os
import sys
import pickle
import numpy as np
import pandas as pd

try:
    np_arr_1 = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/projecta/pickles/np_arr_1.pkl", "rb" ))
    np_arr_2 = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/projecta/pickles/np_arr_2.pkl", "rb" ))
except Exception as e:
    print("failed to load pickle, ignored")
    print(str(e))

## $xprparam_projectname = projecta 
## $xprparam_componentname = first_component
## $xprparam_componenttype = pipeline_job
## $xprparam_componentflavor = python
## $xprparam_global_variables = ["np_arr_1", "np_arr_2"]

"""
This is the implementation of the First Component using numpy
"""



np_arr_1 = np.arange(100).reshape(10, 10)
print(np_arr_1)

np_arr_2 = np.arange(200).reshape(25, 8)
print(np_arr_2)


try:
    pickle.dump(np_arr_1, open("/mnt/nfs/data/jupyter_experiments/projects/projecta/pickles/np_arr_1.pkl", "wb"))
    pickle.dump(np_arr_2, open("/mnt/nfs/data/jupyter_experiments/projects/projecta/pickles/np_arr_2.pkl", "wb"))
except Exception as e:
    print(str(e))
