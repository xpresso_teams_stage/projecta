"""
Xpresso Controller cli to process argument parser
"""
__all__ = ['XpressoControllerCLI']
__author__ = 'Sahil Malav'

import copy
import json
import os
import sys
import warnings
from datetime import datetime as datetime_lib

import click

from xpresso.ai.client.cli_response_formatter import \
    CLIResponseFormatter
from xpresso.ai.client.controller_client \
    import ControllerClient
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    CLICommandFailedException, ControllerClientResponseException
from xpresso.ai.core.commons.utils import constants
from xpresso.ai.core.commons.utils.generic_utils import str_hash256
from xpresso.ai.core.commons.utils.constants import help_option_short, \
    help_option_long, version_option_full, config_paths
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.logging.xpr_log import XprLogger

# We are doing this to suppress all warning from the python clients
warnings.simplefilter("ignore")  # Change the filter in this process
os.environ["PYTHONWARNINGS"] = "ignore"

HTML_CLI_INPUT = "html"


class XpressoControllerCLI:
    """
    It takes the command line arguments and processes it as needed. xprctl
    binary uses this class to serve the command
    """

    COMMAND_KEY_WORD = "command"
    USER_ARGUMENT = "user"
    WORKSPACE_ARGUMENT = "workspace"
    FILE_ARGUMENT = "file"
    INPUT_ARGUMENT = "input"
    CONTROLLER_SECTION = 'controller'
    CLIENT_PATH = 'client_path'
    VERBOSE_ARGUMENT = "verbose"

    format_mapping = {
        "get_experiment_details": CLIResponseFormatter.PRINT_FORMAT_EXPERIMENT_TABULAR,
        "compare_experiments": CLIResponseFormatter.PRINT_FORMAT_EXPERIMENT_TABULAR,
    }

    verbose_parameter = ["True", "true"]

    def __init__(self):
        self.command = None
        self.arguments = {}

        # Setup initial commands
        self.info_commands = {}
        self.access_commands = {}
        self.profile_management = {}
        self.user_management = {}
        self.project_management = {}
        self.experiment_management = {}
        self.version_management = {}
        self.data_version_management = {}
        self.administration_commands = {}
        self.supported_commands = {}
        self.initialize_commands()

        config_path = XprConfigParser.DEFAULT_CONFIG_PATH
        self.config = XprConfigParser(config_path)
        self.path = os.path.join(
            os.path.expanduser('~'),
            self.config[self.CONTROLLER_SECTION][self.CLIENT_PATH])
        self.workspace_file = f'{self.path}.workspace'
        config = self.get_config_path(constants.config_path_flag)
        self.controller_client = ControllerClient(config_path=config)
        self.logger = XprLogger(config_path=config)

    def get_config_path(self, flag):
        """
        Fetches config path / log config path corresponding to particular
        workspace
        Args:
            flag : indicates whether to fetch config path or log config path
        Returns: corresponding config path

        """
        if not os.path.isdir(self.path):
            os.makedirs(self.path, constants.permission_755)
        if not os.path.isfile(self.workspace_file):
            with open(self.workspace_file, 'w+') as f:
                f.write('default')
        with open(self.workspace_file, 'r') as f:
            workspace = f.read()
            workspace = workspace.strip(' \n\r')
        if workspace not in config_paths:
            workspace = 'default'
        config_path = config_paths[workspace]
        return config_path

    def initialize_commands(self):
        """
        Creates a mapping ot command to functions
        """
        try:
            self.access_commands = {
                "login": self.login,
                "logout": self.logout
            }
            self.supported_commands.update(self.access_commands)

            self.profile_management = {
                "update_password": self.update_password
            }

            self.supported_commands.update(self.profile_management)

            self.user_management = {
                "register_user": self.register_user,
                "modify_user": self.modify_user,
                "deactivate_user": self.deactivate_user,
                "get_users": self.get_users
            }
            self.supported_commands.update(self.user_management)

            self.project_management = {
                "create_project": self.create_project,
                "modify_project": self.modify_project,
                "get_project": self.get_project,
                "get_projects": self.get_project,
                "build_project": self.build_project,
                "get_build_version": self.get_build_version,
                "get_build_versions": self.get_build_version,
                "deploy_project": self.deploy_project,
                "get_pipeline_versions": self.get_pipeline_versions,
                "undeploy_project": self.undeploy_project,
                "deactivate_project": self.deactivate_project,
                "submit_notebook": self.submit_notebook
            }

            self.supported_commands.update(self.project_management)

            self.experiment_management = {
                "get_experiment_details": self.get_experiment_details,
                "start_experiment": self.start_experiment,
                "pause_experiment": self.pause_experiment,
                "restart_experiment": self.restart_experiment,
                "terminate_experiment": self.terminate_experiment,
                "compare_experiments": self.compare_experiments
            }

            self.supported_commands.update(self.experiment_management)

            self.version_management = {
                "update": self.update,
                "version": self.get_version
            }
            self.supported_commands.update(self.version_management)

            self.data_version_management = {
                "list_repo": self.list_repo,
                "create_branch": self.create_branch,
                "create_repo": self.create_repo,
                "push_dataset": self.push_dataset,
                "list_dataset": self.list_dataset,
                "pull_dataset": self.pull_dataset
            }
            self.supported_commands.update(self.data_version_management)

            self.administration_commands = {
                "get_clusters": self.get_clusters,
                "get_cluster": self.get_clusters,
                "register_cluster": self.register_cluster,
                "deactivate_cluster": self.deactivate_cluster,
                "get_nodes": self.get_nodes,
                "register_node": self.register_node,
                "deactivate_node": self.deactivate_node,
                "provision_node": self.provision_node,
                "assign_node": self.assign_node,
            }
            self.supported_commands.update(self.administration_commands)

            self.info_commands = {
                "info": self.info,
                "list": self.list_supported_commands
            }
            self.supported_commands.update(self.info_commands)

        except AttributeError:
            raise CLICommandFailedException("CLI issue. Contact developer to "
                                            "fix it.")

    def info(self):
        """
        Gives links for bitbucket and other platform
        :return:
        """
        return self.controller_client.url_info()

    def extract_argument(self, argument):
        if argument in self.arguments:
            return self.arguments[argument]
        return None

    def extract_json_from_file_or_input(self):
        """
        Extracts json data from either file or input
        """
        file_fs = self.extract_argument(self.FILE_ARGUMENT)
        input_json = self.extract_argument(self.INPUT_ARGUMENT)
        if input_json:
            try:
                data = json.loads(input_json)
            except json.JSONDecodeError as e:
                raise CLICommandFailedException(
                    f"Invalid JSON file provided as input. \n"
                    f"Error on line number {e.lineno} : "
                    f"'{e.msg}'.\n"
                    f"Please check your file's formatting.")
        elif file_fs:
            try:
                data = json.load(file_fs)
            except json.JSONDecodeError as e:
                raise CLICommandFailedException(
                    f"Invalid JSON file provided as input. \n"
                    f"Error on line number {e.lineno} : "
                    f"'{e.msg}'.\n"
                    f"Please check your file's formatting.")
        else:
            raise CLICommandFailedException(
                "Please provide input json using "
                "-f/--file or -i/--input")
        return data

    def execute(self, **kwargs):
        """
        Validates the command provided and calls the relevant function for
        execution

        Args:
            kwargs: It takes kwargs as argument which should contain the
                    argument passed in command line
        """
        self.arguments = kwargs
        if self.COMMAND_KEY_WORD not in self.arguments:
            raise CLICommandFailedException("No valid command provided."
                                            "Please type xprctl list for "
                                            "complete list of commands")

        command = self.arguments[self.COMMAND_KEY_WORD]

        if command not in self.supported_commands:
            raise CLICommandFailedException(f"{command} not supported")

        try:
            self.logger.info(f"executing command {command}"
                             f" with argument {self.arguments}")
            response = self.supported_commands[command]()
            self.logger.info(f"Command executed with response {response}")
            format_type = CLIResponseFormatter.PRINT_FORMAT_HIERARCHICAL
            if command in self.format_mapping:
                format_type = self.format_mapping[command]
            return response, format_type
        except TypeError as e:
            self.logger.error(e)
            raise CLICommandFailedException(f"{command} is not executable")

    def list_supported_commands(self):
        data_version_exclusion_list = {
            "create_repo": True
        }
        experiment_management_exclusion_list = {
        }
        return [
            {"Access": list(self.access_commands.keys())},
            {"Profile Management": list(self.profile_management.keys())},
            {"User Management": list(self.user_management.keys())},
            {"Project Management": list(self.project_management.keys())},
            {"Experiment & Run Management": [cmd for cmd in list(self.experiment_management.keys())
                                             if cmd not in experiment_management_exclusion_list]
             },
            {"CLI version management": list(self.version_management.keys())},
            {"Data version management": [cmd
                                         for cmd in list(
                    self.data_version_management.keys())
                                         if
                                         cmd not in data_version_exclusion_list]},
            {"Information": list(self.info_commands.keys())},
        ]

    def save_workspace(self, username, workspace):
        """
        Saves the value of workspace in a local file
        Args:
            username: username
            workspace: workspace

        Returns: nothing

        """
        try:
            with open(self.workspace_file, 'w+') as file:
                file.write(workspace)
            self.workspace_file_base = os.path.join(self.path, str_hash256(
                username))
            self.workspace_file = os.path.join(self.workspace_file_base,
                                               '.workspace')
            if not os.path.isdir(self.workspace_file_base):
                os.makedirs(self.workspace_file_base, 0o755)
            with open(self.workspace_file, 'w+') as file:
                file.write(workspace)
            self.logger.info('CLIENT : Workspace written to file. Exiting.')
        except (IOError, OSError):
            self.logger.error('CLIENT : Error occured while saving workspace.')
            raise CLICommandFailedException('Failed to save your workspace. '
                                            'Please contact a developer.')

    @staticmethod
    def input_prompt_caller(prompt_string: str, timeout: int,
                            hide_input: bool = False):
        """
        prompts the user to provide input for 'prompt_string'

        Args:
            prompt_string: prompt to be displayed
            timeout: timeout for user input
            hide_input: flag to hide or show the input
                while being entered by user
        Returns:
            str: user input value if entered before timeout
        """
        start_time = datetime_lib.now()
        input_value = click.prompt(prompt_string, hide_input=hide_input,
                                   default=constants.DEFAULT_PASSWORD_VALUE, show_default=False)
        if input_value == constants.DEFAULT_PASSWORD_VALUE:
            raise CLICommandFailedException("Empty Field was found")
        end_time = datetime_lib.now()
        time_taken = (end_time - start_time).total_seconds()
        if timeout and time_taken > timeout:
            raise CLICommandFailedException(
                f"Time out! Input not entered for too long. "
                f"Please try again and provide input "
                f"within stipulated time of {timeout} seconds."
            )
        return input_value

    def login(self):
        username = self.extract_argument(self.USER_ARGUMENT)
        workspace = self.extract_argument(self.WORKSPACE_ARGUMENT)
        if not workspace:
            raise CLICommandFailedException(
                'xprctl login has been updated. You have to provide '
                'the workspace into which you want to log in. '
                'Eg. xprctl login -w prod. '
                'Execute "xprctl --help" for more info.')
        if workspace not in config_paths.keys():
            raise CLICommandFailedException(
                'Please provide a valid workspace. '
                'Execute "xprctl --help" for more info.')
        if not username:
            username = self.input_prompt_caller("Username", 0)
        self.save_workspace(username, workspace)

        self.controller_client = ControllerClient(config_paths[workspace])
        password_attempt_count = 3
        while password_attempt_count:
            try:
                password = self.input_prompt_caller("Password", timeout=180,
                                                    hide_input=True)
                response = self.controller_client.login(username, password)
                return response
            except ControllerClientResponseException as login_exc:
                password_attempt_count -= 1
                if not password_attempt_count:
                    raise login_exc
                CLIResponseFormatter().pretty_print(data=login_exc.message,
                                                    is_error=True)
        raise CLICommandFailedException("Login attempt failed")

    def sso_login(self):
        response = self.controller_client.sso_login()
        if "url" in response:
            message = f"Login here: {response['url']}"
            CLIResponseFormatter().pretty_print(data=message)
            print("Waiting for login to get successful...")
            return self.controller_client.sso_validate(
                response["validation_token"])
        return response

    def logout(self):
        return self.controller_client.logout()

    def get_clusters(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.get_clusters(data)

    def deactivate_cluster(self):
        data = self.extract_json_from_file_or_input()
        if self.confirm_changes():
            return self.controller_client.deactivate_cluster(data)

    def register_cluster(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.register_cluster(data)

    def get_users(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.get_users(data)

    def register_user(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.register_user(data)

    def modify_user(self):
        data = self.extract_json_from_file_or_input()
        if self.confirm_changes():
            return self.controller_client.modify_user(data)

    def deactivate_user(self):
        data = self.extract_json_from_file_or_input()
        if self.confirm_changes():
            return self.controller_client.deactivate_user(data)

    def register_node(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.register_node(data)

    def get_nodes(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.get_nodes(data)

    def provision_node(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.provision_node(data)

    def deactivate_node(self):
        data = self.extract_json_from_file_or_input()
        if self.confirm_changes():
            return self.controller_client.deactivate_node(data)

    def assign_node(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.assign_node(data)

    def create_project(self):
        print("Creating new project. It may take several seconds...")
        data = self.extract_json_from_file_or_input()
        return self.controller_client.create_project(data)

    def get_project(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.get_project(data)

    def deactivate_project(self):
        data = self.extract_json_from_file_or_input()
        if self.confirm_changes():
            return self.controller_client.deactivate_project(data)

    def submit_notebook(self):
        self.logger.info("Submitting the notebook. It may take several "
                         "seconds...")
        file_fs = self.extract_argument(self.FILE_ARGUMENT)
        notebook_ext = os.path.splitext(file_fs.name)[1]
        if notebook_ext is None:
            raise CLICommandFailedException(
                "Invalid notebook file. Path provided is of a directory."
            )
        if notebook_ext != '.ipynb':
            raise CLICommandFailedException(
                "Invalid notebook file. File provided is not a Jupyter Notebook."
            )
        data = self.extract_json_from_file_or_input()
        return self.controller_client.submit_notebook(data)

    def confirm_changes(self):
        """
        prompts the user to provide input for confirming changes
        Returns:
            Successful outcome in case of confirmation else exception
        """
        confirm_changes = self.input_prompt_caller(
            "Confirm Changes(yes/y)", timeout=60, hide_input=False
        )
        if confirm_changes.lower() == "yes" or confirm_changes.lower() == "y":
            return True
        else:
            raise CLICommandFailedException(
                f"No changes done."
            )

    def modify_project(self):
        data = self.extract_json_from_file_or_input()
        if self.confirm_changes():
            print("Modifying existing project. It may take several seconds...")
            return self.controller_client.modify_project(data)

    def build_project(self):
        print(
            "Submitting build for the project. It may take several seconds...")
        data = self.extract_json_from_file_or_input()
        return self.controller_client.build_project(data)

    def get_build_version(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.get_build_version(data)

    def deploy_project(self):
        print("Deploying project. It may take several seconds...")
        data = self.extract_json_from_file_or_input()
        return self.controller_client.deploy_project(data)

    def undeploy_project(self):
        data = self.extract_json_from_file_or_input()
        if self.confirm_changes():
            return self.controller_client.undeploy_project(data)

    def get_pipeline_versions(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.get_pipeline_versions(data)

    def get_version(self):
        return self.controller_client.fetch_version()

    def update(self):
        return self.controller_client.update_xpresso()

    def update_password(self):
        username = self.extract_argument(self.USER_ARGUMENT)
        if not username:
            username = self.input_prompt_caller("Username", 0)
        old_password = self.input_prompt_caller("Current Password", timeout=60,
                                                hide_input=True)
        new_password = self.input_prompt_caller("New Password", timeout=60,
                                                hide_input=True)
        confirm_password = self.input_prompt_caller(
            "Confirm New Password:", timeout=60, hide_input=True
        )
        if new_password != confirm_password:
            raise CLICommandFailedException("Passwords do not match!")

        return self.controller_client.update_password(
            {
                "uid": username,
                "old_pwd": old_password,
                "new_pwd": new_password
            }
        )

    def create_repo(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.create_repo(data)

    def list_repo(self):
        return self.controller_client.list_repos()

    def create_branch(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.create_branch(data)

    def push_dataset(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.push_dataset(data)

    def pull_dataset(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.pull_dataset(data)

    def list_dataset(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.list_dataset(data)

    def start_experiment(self):
        print("Starting your experiment. It may take few seconds ...")
        data = self.extract_json_from_file_or_input()
        return self.controller_client.start_experiment(data)

    def get_experiment_details(self):
        verbose_factor = self.extract_argument(self.VERBOSE_ARGUMENT)
        if verbose_factor and verbose_factor not in self.verbose_parameter:
            raise CLICommandFailedException(
                'Please provide a valid verbose parameter. '
                'Execute "xprctl --help" for more info.')
        data = self.extract_json_from_file_or_input()
        return self.controller_client.get_experiment_details(data, verbose_factor)

    def convert_to_tabular_format(self, response):
        """
        Converts a dictionary representation to a tabular format for a better
        visualization experience
        Args:
            response(dict): dictionary based results

        Returns:
            dict: contains the data in a tabular format
        """

    def compare_experiments(self):
        verbose_factor = self.extract_argument(self.VERBOSE_ARGUMENT)
        if verbose_factor and verbose_factor not in self.verbose_parameter:
            raise CLICommandFailedException(
                'Please provide a valid verbose parameter. '
                'Execute "xprctl --help" for more info.')
        data = self.extract_json_from_file_or_input()
        data.update({"verbose": verbose_factor})
        return self.controller_client.compare_experiments(data)

    def pause_experiment(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.pause_experiment(data)

    def restart_experiment(self):
        data = self.extract_json_from_file_or_input()
        return self.controller_client.restart_experiment(data)

    def terminate_experiment(self):
        data = self.extract_json_from_file_or_input()
        if self.confirm_changes():
            return self.controller_client.terminate_experiment(data)


class XpressoClickCommand(click.Command):
    """ Overriding click.command to give us more control over the click
    output and behaviour"""
    _original_args = None

    def make_context(self, info_name, args, parent=None, **extra):
        """ Override make context so that we can capture all exceptions"""
        # grab the original command line arguments
        self._original_args = ' '.join(args)
        possible_args = copy.deepcopy(args)
        try:
            return super().make_context(
                info_name, args, parent=parent, **extra)
        except click.ClickException as exc:
            if not exc.ctx:
                click.secho(f'Error: {exc.format_message()}', fg="red")
                sys.exit(exc.exit_code)

            if (help_option_short in possible_args or
                    help_option_long in possible_args):
                self.show_help(exc)
            if version_option_full in possible_args:
                self.show_version()
            self.handle_exception(exc=exc)

    def invoke(self, ctx):
        """ Override invoke so that we can capture all exceptions"""
        try:
            return super().invoke(ctx)
        except click.ClickException as exc:
            self.handle_exception(exc=exc)

    def show_help(self, exc):
        """ Show help message and xits from here
        Argument:
            exc: ClickException object
        """
        click.echo(self.get_help(exc.ctx))
        sys.exit(0)

    @staticmethod
    def show_version():
        """ Show version and exit from here"""
        xprctl = XpressoControllerCLI()
        CLIResponseFormatter().pretty_print(data=xprctl.get_version())
        sys.exit(0)

    def handle_exception(self, exc):
        """
        Handle the exception in proper way. Exits from here
        Args:
            exc: ClickException object
        """
        usage = self.get_usage(exc.ctx)
        click.echo(usage)
        hint = ('Try "%s %s" for help.\n'
                % (exc.ctx.command_path, exc.ctx.help_option_names[0]))
        click.echo(hint)
        click.secho(f'Error: {exc.format_message()}', fg="red")
        sys.exit(exc.exit_code)


@click.command(cls=XpressoClickCommand)
@click.argument('command')
@click.option('-h', '--help', is_flag=True)
@click.option('--version', is_flag=True)
@click.option('-f', '--file', type=click.File(),
              help='Path of the file you want to use as input')
@click.option('-u', '--user', type=str, help='Username')
@click.option('-i', '--input', type=str,
              help='Quick Input. Type a json stub as a string to pass it as '
                   'input. Removes the need to use -f.'
                   'Eg. -i \'{"uid": "abc"}\' ')
@click.option('-w', '--workspace', type=str,
              help='Used to select the workspace while logging in. '
                   'Possible values are : dev, qa, prod, sandbox.')
@click.option('--verbose', type=str,
              help='Used as a parameter to compare experiments'
                   'Eg --verbose True')
@click.option('--html', is_flag=True,
              help="It prints th output in the html format")
@click.pass_context
def cli_options(ctx, **kwargs):
    """ xprctl is cli for xpresso controller. It can be used to create, build,
    deploy and manage the projects in the xpresso infrastructure.

    Try "xprctl -h/--help" for help. """
    logger = XprLogger("xprctl")
    xprctl = XpressoControllerCLI()
    try:
        response, format_type = xprctl.execute(**kwargs)
        if response:
            output_type = CLIResponseFormatter.OUTPUT_TYPE_PLAIN_TEXT
            if HTML_CLI_INPUT in kwargs and kwargs[HTML_CLI_INPUT]:
                output_type = CLIResponseFormatter.OUTPUT_TYPE_HTML
            CLIResponseFormatter().pretty_print(data=response,
                                                format_type=format_type,
                                                output_type=output_type)
        sys.exit(0)
    except (ControllerClientResponseException,
            CLICommandFailedException) as cli_error:
        CLIResponseFormatter().pretty_print(data=cli_error.message,
                                            is_error=True)
        sys.exit(1)
    except Exception as e:
        logger.exception(e)
        click.secho("The server is currently unavailable. "
                    "Please try after some time. "
                    "Contact the System Administrator if you continue to "
                    "have problems.", err=True, fg="red")
        sys.exit(1)


if __name__ == "__main__":
    cli_options()
