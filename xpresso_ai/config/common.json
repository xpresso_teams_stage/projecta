{
    "env": "stage",
    "general": {
        "package_name": "xpresso.ai",
        "package_path": "/opt/xpresso.ai"
    },
    "structure": {
        "library_structure_list": [
            "samples",
            "Makefile",
            "ReadMe.md",
            "Release.md",
            "requirements.txt",
            "setup.py",
            "VERSION",
            "xpresso/__init__.py",
            "xpresso/ai/__init__.py",
            "xpresso/ai/client",
            "xpresso/ai/core",
            "xprbuild/system/linux/install_alluxio.sh"
        ]
    },
    "ssl": {
        "cert_path": "config/docker-distribution/certs/wildcard.xpresso.ai.cert",
        "pkey_path": "config/docker-distribution/certs/wildcard.xpresso.ai.key",
        "verify": false
    },
    "bundles_setup": {
        "nfs": {
            "subnet_to_nfs_map": {
                "nfs1.xpresso.ai": "10.*.*.*",
                "nfs2.xpresso.ai": "172.*.*.*"
            },
            "mount_location": "/mnt/nfs/data",
            "nfs_location": "/mnt/exports/vmdata1/xpresso_platform_sb"
        },
        "base_bundle_dependency": {
            "dependency_config_file": "config/dependency_config.json"
        },
        "option_bundle_dependency": {
            "dependency_config_file": "config/option_bundle_dependency.json"
        },
        "apt-get-repo": {
            "package_root_folder": "/mnt/nfs/data/admin/packages",
            "public_key_file": "${package_root_folder}/secret/esx-gpg-public.key",
            "private_key_file": "${package_root_folder}/secret/esx-gpg-private.key",
            "hosted_package": "${package_root_folder}/hosted_packages",
            "package-list": "scripts/linux/ubuntu/apt-get-repo/full-package-list.txt",
            "meta_packages_folder": "config/apt-get/metapackages",
            "docker-name": "apt-get-repo",
            "dockerfile-path": "scripts/docker/apt-get-repo/apt-mirror.dockerfile"
        },
        "kubernetes": {
            "pod_network_cidr": "10.244.0.0/16"
        },
        "base_ubuntu": {
            "pkg_list": [
                "build-essential",
                "vim",
                "curl",
                "python3-dev",
                "python3.7-dev",
                "python-dev",
                "libcurl4-openssl-dev",
                "libssl-dev",
                "libffi-dev",
                "git",
                "wget",
                "tmux",
                "libfreetype6",
                "apt-transport-https",
                "ca-certificates",
                "software-properties-common",
                "locales",
                "pkg-config",
                "nano",
                "cmake",
                "libxml2-dev",
                "libxmlsec1-dev",
                "checkinstall",
                "libsasl2-dev",
                "libldap2-dev"
            ]
        },
        "docker_distribution": {
            "harbor_cfg_file": "config/harbor/harbor.cfg",
            "harbor_compose_file": "config/harbor/docker-compose.yml",
            "harbor_folder": "/opt/harbor",
            "docker_distribution_folder": "/opt/docker-distribution",
            "docker_distribution_local_folder": "config/docker-distribution"
        },
        "development_vm": {
            "requirement_file": "config/development_vm/requirements.txt"
        },
        "kong": {
            "host_mount_path": "/mnt/nfs/data/xpresso_platform/external/kong"
        },
        "pachyderm": {
            "minio_spec_path": "config/pachyderm_setup/minio_setup",
            "minio_namespace": "minio-pachyderm",
            "minio_pv_nfs_mount_path": "/mnt/exports/vmdata1/xpresso_platform_ENV/pachyderm/minio",
            "pachyderm_namespace": "pachyderm",
            "pachctl_pkg_link": "https://github.com/pachyderm/pachyderm/releases/download/v1.9.0/pachctl_1.9.0_amd64.deb",
            "pachyderm_spec_path": "config/pachyderm_setup/pachyderm_setup.yaml",
            "release_name": "pachyderm-release",
            "pachyderm_etcd_path": "config/pachyderm_setup/pachyderm-etcd-pv.yaml",
            "pachyderm_etcd_pv_nfs_mount_path": "/mnt/exports/vmdata1/xpresso_platform_ENV/pachyderm/etcd"
        }
    },
    "connectors": {
        "presto": {
            "presto_ip": "172.16.6.2",
            "presto_port": 8082,
            "presto_user": "root",
            "DSN": {
                "testing_mysql": {
                    "catalog": "mysql",
                    "schema": "presto"
                },
                "testing_cassandra": {
                    "catalog": "cassandra",
                    "schema": "test_db"
                },
                "testing_sqlserver": {
                    "catalog": "sqlserver",
                    "schema": "dbo"
                },
                "testing_mongodb": {
                    "catalog": "mongo",
                    "schema": "test"
                }
            }
        },
        "alluxio": {
            "alluxio_ip": "172.16.6.2",
            "alluxio_port": "39999"
        },
        "hdfs": {
            "hdfs_ip": "172.16.1.81",
            "hdfs_port": "8020"
        }
    },
    "jupyter_experiments": {
        "mount_path": "/mnt/nfs/data/jupyter_experiments/notebooks",
        "notebook_projects_base_path": "/mnt/nfs/data/jupyter_experiments/projects",
        "json_template_folder": "xpresso/ai/server/controller/jupyter_experiment/json"
    },
    "controller": {
        "server_url": "https://0.0.0.0:5050",
        "client_path": ".xpr/",
        "soft_expiry": 1800,
        "hard_expiry": 86400
    },
    "mongodb": {
        "mongo_url": "mongodb://172.16.6.1:27017/?replicaSet=rs0",
        "local_mongo_url": "mongodb://localhost:27017/?replicaSet=rs0",
        "database": "xprdb",
        "mongo_uid": "xprdb_admin",
        "mongo_pwd": "xprdb@Abz00ba",
        "w": 1,
        "backup_directory": "/mnt/nfs/data/mongo",
        "backup_interval": 5,
        "interval_between_retries": 5,
        "max_retries": 2,
        "experiment_collection": "experiments",
        "run_collection": "runs",
        "index": {
            "runs": {
                "key_name": [
                    [
                        "run_name",
                        1
                    ]
                ],
                "index_name": "run_name"
            },
            "projects": {
                "key_name": [
                    [
                        "name",
                        1
                    ]
                ],
                "index_name": "project_name"
            },
            "pipelines": {
                "key_name": [
                    [
                        "pipelines.name",
                        1
                    ]
                ],
                "index_name": "pipeline_name"
            },
            "experiments": {
                "key_name": [
                    [
                        "experiment name",
                        1
                    ]
                ],
                "index_name": "experiment_name"
            },
            "users": {
                "key_name": [
                    [
                        "uid",
                        1
                    ]
                ],
                "index_name": "user_name"
            }
        }
    },
    "vms": {
        "username": "root",
        "password": "abz00ba1nc#123",
        "guest_login": {
            "username": "xpr_guest",
            "password": "E50mX6HaIZ#",
            "group": "xpr_guest"
        }
    },
    "build_management": {
        "build_tool": "jenkins",
        "jenkins": {
            "master_host": "http://172.16.6.51:8080",
            "username": "xpradmin",
            "password": "Xpresso@Abz00ba2019#@!",
            "template_job": "template-pipeline"
        }
    },
    "code_management": {
        "code_manager_tool": "bitbucket",
        "skeleton_path": "http://bitbucket.org/abzooba-screpo/skeleton-build.git",
        "bitbucket": {
            "restapi": "https://api.bitbucket.org/2.0",
            "teamname": "xpresso_teams_sandbox",
            "username": "bbdevadm",
            "password": "#Abzbbdevadm@2019",
            "email": "bitbucketdevadm@abzooba.com"
        }
    },
    "ldap": {
        "ldap_url": "ldap://172.16.6.1:30389"
    },
    "authentication_type": "ldap",
    "component_deployment_manager_flavor": "kubernetes",
    "kubernetes": {
        "secure_port": "6443"
    },
    "kubeflow": {
        "secure_port": "6443",
        "dashboard_port": "31380",
        "pv_mount_location_prefix": "/mnt/nfs/data/projects"
    },
    "istio": {
        "secure_port": "6443",
        "dashboard_info_suffix": "/d/yYViFIEZk/xpresso-service-mesh"
    },
    "projects": {
        "deployment_files_folder": "deployment_files",
        "declarative_pipeline_folder": "deployment_files/declarative_pipeline_files",
        "kubeflow_template": "xpresso/ai/server/controller/project_management/kubeflow/declarative_pipeline/kubeflow_template",
        "valid_environments": [
            "DEV",
            "INT",
            "QA",
            "UAT",
            "PROD"
        ]
    },
    "email_notification": {
        "smtphost": "smtp.gmail.com",
        "smtpport": 587,
        "sender_mail": "##",
        "sender_passwd": "##"
    },
    "gateway": {
        "provider": "kong",
        "admin_url": "http://172.16.3.1:8001",
        "proxy_url": "http://172.16.3.1:8000"
    },
    "pachyderm_server": {
        "cluster_ip": "172.16.3.51",
        "port": 30650
    },
    "docker_registry": {
        "host": "dockerregistry.xpresso.ai",
        "username": "admin",
        "password": "Abz00ba@123"
    },
    "visualization": {
        "logo_path": "static/logo.png"
    },
    "distributed_exploration": {
        "hdfs_ip": "172.16.1.81",
        "ui_port": "50070",
        "ipc_port": "8020",
        "folder_path": "/user/xprops/DistributedStructuredDataset"
    },
    "logging": {
        "project_name": "default",
        "logstash": {
            "url": "http://172.16.3.51:30050"
        },
        "kibana": {
            "url": "http://172.16.3.51:30560"
        },
        "elastic_search": {
            "url": "http://172.16.3.51:30920"
        },
        "log_handler": {
            "host": "172.16.3.51",
            "port": 30050,
            "formatter": {
                "filename": true,
                "funcName": true,
                "levelname": true,
                "levelno": true,
                "lineno": true,
                "module": true,
                "pathname": true,
                "process": true,
                "processName": true,
                "thread": true,
                "threadName": true,
                "msg": true,
                "exc_info": true,
                "stack_info": true
            },
            "log_to_console": false,
            "log_to_file": true,
            "log_to_elk": true,
            "find_config_recursive": false,
            "cache_in_file": true,
            "logs_folder_path": "~/.xpr/logs",
            "log_level": "INFO",
            "default_folder_path": "~/.xpr/logs"
        }
    },
    "data_versioning": {
        "tool": "pachyderm",
        "server": {
            "cluster_ip": "172.16.3.51",
            "port": 30650
        }
    },
    "spark_cluster": {
        "cluster_admin": "xpresso",
        "default_resource_limits": {
            "driver_memory": "512m",
            "executor_memory": "512m",
            "executor_cores": 1,
            "num_executors": 1
        },
        "xpresso": {
            "cluster_manager": "k8s",
            "k8s": {
                "master": {
                    "host": "172.16.1.81",
                    "user": "root",
                    "spark_home": "/opt/spark/spark-2.4.4-bin-hadoop2.7",
                    "service_account": "spark",
                    "secret": "dockerkey",
                    "job_ui_port": 30252,
                    "port": 6443
                }
            },
            "yarn": {
                "master": {
                    "host": "172.16.1.81",
                    "user": "root",
                    "job_ui_port": 8088,
                    "spark_home": "/usr/hdp/3.1.4.0-315/spark2"
                }
            }
        }
    },
    "pyspark": {
        "secure_port": "22"
    },
    "spark": {
        "secure_port": "22"
    },
    "dashboard": {
        "bitbucket": "https://bitbucket.org/",
        "kubernetes": "https://172.16.6.51:30252/",
        "kibana": "http://172.16.6.1:30560/",
        "jenkins": "http://172.16.6.1:8080",
        "nfs": "172.16.0.20",
        "kubeflow": "http://172.16.6.51:31380"
    },
    "jenkins": {
        "master_host": "http://172.16.3.51:8080"
    }
}