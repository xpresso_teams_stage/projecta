import os
import sys
import pickle
import numpy as np
import pandas as pd

try:
    np_arr_1 = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/projecta/pickles/np_arr_1.pkl", "rb" ))
    np_arr_2 = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/projecta/pickles/np_arr_2.pkl", "rb" ))
    global_df_1 = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/projecta/pickles/global_df_1.pkl", "rb" ))
    global_df_2 = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/projecta/pickles/global_df_2.pkl", "rb" ))
except Exception as e:
    print("failed to load pickle, ignored")
    print(str(e))

## $xprparam_projectname = projecta
## $xprparam_componentname = second_component
## $xprparam_componenttype = pipeline_job
## $xprparam_componentflavor = python
## $xprparam_global_variables = ["np_arr_1", "np_arr_2", "global_df_1", "global_df_2"]

"""
This is the implementation of the Second Component using pandas
"""

__author__ = "xpresso.ai"



global_df_1 = pd.DataFrame(data=np_arr_1)
print(global_df_1)

global_df_2 = pd.DataFrame(
    {
        'num_legs': [2, 4, 8, 0],
        'num_wings': [2, 0, 0, 0],
        'num_specimen_seen': [10, 2, 1, 8]
    }, 
    index=['falcon2', 'dog2', 'spider2', 'fish2']
)
print(global_df_2)

try:
    pickle.dump(np_arr_1, open("/mnt/nfs/data/jupyter_experiments/projects/projecta/pickles/np_arr_1.pkl", "wb"))
    pickle.dump(np_arr_2, open("/mnt/nfs/data/jupyter_experiments/projects/projecta/pickles/np_arr_2.pkl", "wb"))
    pickle.dump(global_df_1, open("/mnt/nfs/data/jupyter_experiments/projects/projecta/pickles/global_df_1.pkl", "wb"))
    pickle.dump(global_df_2, open("/mnt/nfs/data/jupyter_experiments/projects/projecta/pickles/global_df_2.pkl", "wb"))
except Exception as e:
    print(str(e))
